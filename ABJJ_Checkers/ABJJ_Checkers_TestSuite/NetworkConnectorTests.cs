﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ABJJ_Checkers;
using System.Threading.Tasks;

namespace ABJJ_Checkers_TestSuite
{
    [TestClass]
    public class NetworkConnectorTests
    {
        [TestMethod]
        //TODO: If these are not async, then testing cannot be done
        //TODO: atleast without creating two seperate instances? but even then i think they still need to be async.
        public void TestMethod1()
        {
            NetworkConnector host = new NetworkConnector();
            NetworkConnector join = new NetworkConnector();
            var ip = host.GetIPAddress();

            Task<bool> T = host.HostGame();

            join.JoinGame(ip);
            //Wait for HostGame() to finish
            T.Wait();
            string testMove = "TestMove";
            host.SendMove(testMove);
            string output = join.ReadMove();
            Assert.AreEqual(testMove,output);
            join.SendMove(output);
            string output2 = host.ReadMove();
            Assert.AreEqual(output, output2);
            host.CloseGame();
            join.CloseGame();


            
        }//connector.
    }
}
