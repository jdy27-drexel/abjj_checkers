﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace ABJJ_Checkers
{
    //Serializable so we can pass board over network
    [Serializable]
    public class Checker
    {
        #region Variables
        //X cordinate of the checker
        public int x;

        //Y cordinate of the checker
        public int y;

        //If there is an actual color class we can use, I do not know where it is
        public Color color;

        //id of the checker
        public string sid;

        //boolean stating whether checker is a king
        public bool king;
        #endregion Variables

        //Constructor
        public Checker(int x, int y, Color c)
        {
            this.x = x;
            this.y = y;
            this.color = c;
        }

        //Gets the info for a checker
        public void getCheckerInfo()
        {

        }

        //Sets the location of the checker by changing its x and y values
        public void SetLocation(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        //Sets the color of the checker
        public void setColor(Color color)
        {
            this.color = color;
        }

        //Sets the checker king attibute
        public void setKing(bool king)
        {
            this.king = king;
        }

        //Sets the checkers sid
        public void setSid(string sid)
        {
            this.sid = sid;
        }

    }
}
