﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ABJJ_Checkers
{
    //Main Screen form
    public partial class Main_Screen : Form
    {
        //networkConnector for hosting games
        NetworkConnector hostcon = null;

        //Constructor
        public Main_Screen()
        {
            InitializeComponent();
        }
        //Constructor with Version 
        public Main_Screen(string version)
        {
            InitializeComponent();
            this.VersionLabel.Text = version;
        }

        //OnClick for when the host button is clicked
        //Starts a session, and waits for a user to join
        //Game starts once user joins and popup is clicked out
        private void HostButton_Click(object sender, EventArgs e)
        {
            //Create new hostconn if its not already initalized
            if(hostcon == null)
                hostcon = new NetworkConnector();

            //Get the IP to host on, begin waiting for someone to join
            //And display IP so user can give it to other player
            string IP = hostcon.GetIPAddress();
            Task<bool> host_task = hostcon.HostGame();
            var popup = new IPPopup();
            popup.SetTextboxValue(IP);

            //Wait for other player to join before clearing popup
            while (!host_task.IsCompleted)
            {
                popup.ShowDialog();
            }
            
            //Make sure task is ended and close popup            
            host_task.Wait();          
            popup.Close();

            //Start a new game
            GameDriver driver = new GameDriver(ref hostcon);
            driver.PlayGame();

        }

        //OnClick for when the join button is clicked
        //Attempts to join a session
        private void JoinButton_Click(object sender, EventArgs e)
        {          
            //Setup join network
            NetworkConnector join = new NetworkConnector();
            
            //Setup and display popup to get entered IP
            var popup = new IPPopup();
            string val = "";
            bool joined = false;
            val = popup.GetEnterIP();
            
            //Try to join on the given IP value
            joined = join.JoinGame(val);
            if (joined)
            {
                //Joined the connection, start the game now
                GameDriver driver = new GameDriver(ref join);
                driver.PlayGame();
            }
            
            
        }

        //OnClick for when the Quit button is clicked
        //Closes out the form
        private void QuitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
