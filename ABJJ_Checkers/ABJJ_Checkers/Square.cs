﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ABJJ_Checkers
{
    [Serializable]
    public class Square
    {
        #region Variables
        //X coordinate of the square
        public int x;

        //Y coordinate of the square
        public int y;

        //id of the checker that represents its location, ie A4
        public string id;

        //Boolean value that states whether there is a checker at this square
        public bool checker;

        //Base color of the square
        public Color baseColor;

        //Color of the sqaure during current turn
        public Color curColor;


        #endregion Variables

        //Constructor
        public Square(int x, int y, Color color)
        {
            this.x = x;
            this.y = y;
            this.baseColor = color;
            this.curColor = color;
        }

        //Gets the squares information
        public void getSquareInfo()
        {

        }

        //Set the color of the square
        public void setColor(Color color)
        {
            this.curColor = color;
        }

        //Sets the checker attibute stating whether a checker is located on this square
        public void setChecker(bool checker)
        {
            this.checker = checker;
        }
    }
}
