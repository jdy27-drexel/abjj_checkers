﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ABJJ_Checkers
{
    public class GameDriver
    {
        #region Variables       
        //if there is a winner
        bool winner;

        //true if host, false if not
        bool host;

        //Object used to send and recieve moves
        NetworkConnector network;

        //Form that the game is played on
        Game_Screen game_display;

        //GameHelpers object used for helper functions
        GameHelpers gh;

        #endregion Variables

        //Constructor
        public GameDriver(ref NetworkConnector network)
        {
            this.network = network;
            winner = false;
            host = network.IsHost();
            gh = new GameHelpers();
            if(host)
            {
                game_display = new Game_Screen(gh, Color.Red, true);
            }
            else
            {
                game_display = new Game_Screen(gh, Color.White, false);
            }

        }

        //Constructor used when testing solo
        public GameDriver()
        {
            gh = new GameHelpers();
            game_display = new Game_Screen(gh, Color.White, true);
        }
        

        //Start the game
        public void PlayGame()
        {
            //Setup and display board
            gh.InitalizeCheckerBoard(game_display);
            //Create a task for either host or client
            //that will run on seperate thread
            //"overseeing" moves until someone wins
            if (host)//host makes first move
            {
                Task t1 = new Task(() =>
                {
                    while (!winner)//While no winner
                    {
                        while (game_display.moveToSend == null)
                        {
                            //Wait for serialize byte array to send to other user
                            //"Wait for us to make a complete move"
                        }
                        //Got array, send to other user
                        network.SendMove(game_display.moveToSend);
                        //Check if win
                        winner = gh.checkForWin(Color.White);
                        if (winner)//If win, display popup and close
                        {
                            IPPopup winBox = new IPPopup();
                            winBox.SetTextboxValue("You Won");
                            winBox.ShowDialog();
                            game_display.Close();
                            network.CloseGame();
                            break;
                        }
                        //No win, reset moveToSend and wait for a move to be sent to us
                        game_display.moveToSend = null;
                        byte[] ar = network.ReadMoveBytes();
                        //Process sent move
                        gh.ProcessMove(ar);
                        //Redraw board with new board that we just processed
                        gh.drawBoard(game_display.CreateGraphics());
                        //Check if win
                        winner = gh.checkForWin(Color.Red);
                        if (winner)//If win, display popup and close
                        {
                            IPPopup winBox = new IPPopup();
                            winBox.SetTextboxValue("You Lost");
                            winBox.ShowDialog();
                            game_display.Close();
                            network.CloseGame();
                            break;
                        }
                        //Set turn to be our turn
                        game_display.turn = true;
                    }
                });
                //Start the thread
                t1.Start();
            }
            else //Similar to above thread, except its for joiner not host
            {
                Task t1 = new Task(() =>
                {
                    while (!winner)//While no winner
                    {
                        //Wait for move to be sent to us
                        byte[] ar = network.ReadMoveBytes();
                        //Process the move
                        gh.ProcessMove(ar);
                        //Redraw the board with new internal representation we got send
                        gh.drawBoard(game_display.CreateGraphics());
                        //Check for winner
                        winner = gh.checkForWin(Color.White);
                        if (winner)//If win, display popup and close
                        {
                            IPPopup winBox = new IPPopup();
                            winBox.SetTextboxValue("You Lost");
                            winBox.ShowDialog();
                            game_display.Close();
                            network.CloseGame();
                            break;
                        }
                        //Our turn
                        game_display.turn = true;
                        while (game_display.moveToSend == null)
                        {
                            //Wait for serialize byte array to send to other user
                            //"Wait for us to make a complete move"
                        }
                        //Send move once we can
                        network.SendMove(game_display.moveToSend);
                        //Check for winner
                        winner = gh.checkForWin(Color.Red);
                        if (winner)//If win, display popup and close
                        {
                            IPPopup winBox = new IPPopup();
                            winBox.SetTextboxValue("You Won");
                            winBox.ShowDialog();
                            game_display.Close();
                            network.CloseGame();
                            break;
                        }                        
                        game_display.moveToSend = null;

                    }
                });
                //Start this thread
                t1.Start();
            }
        }
    }
}
