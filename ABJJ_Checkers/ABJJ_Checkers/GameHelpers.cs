﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ABJJ_Checkers
{
    public class GameHelpers
    {
        //the game board to be drawn
        Board board;

        public GameHelpers()
        {
            this.board = new Board();
        }

        //find what square was selected given two coordinates
        public Square getSquare(int x, int y)
        {
            int sqX = (x / 50) * 50;
            int sqY = (y / 50) * 50;

            return board.getSquare(sqX, sqY);
        }

        //get a checker or null given a certian square
        public Checker getChecker(Square s)
        {
            return board.getChecker(s);
        }

        public List<Tuple<Square,Square>> moveValidator(Checker c)
        {

            return board.moveValidator(c);
        }

        //updated a checkers location by moving it from one square to another
        public void updateBoard(Square to, Square from)
        {
            board.updateBoard(to, from);
        }

        //update square s to hold checker c
        public void updateSquare(Square s, Checker c)
        {
            board.updateSquare(s, c);
        }

        //Serialize the board object to send over the network
        public byte[] MakeMove()
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, board.getBoard());
                return ms.ToArray();
            }
        }

        //Recieve serialized board object and create the board from it
        public void ProcessMove(byte[] stream)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(stream, 0, stream.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                board.setBoard((Dictionary<Square,Checker>)binForm.Deserialize(memStream));
            }
        }

        //return the dictionary<Square, Checker> that represents the board
        public Dictionary<Square,Checker> getBoard()
        {
            return board.getBoard();
        }

        //Draw the board on the Game Screen form
        public void drawBoard(Graphics g)
        {
            var blackPen = new Pen(Color.Black, 2);
            Dictionary<Square, Checker> boardDict = board.getBoard();
            foreach (var square in boardDict.Keys)
            {
                DrawSquare(g, square);
                if (square.checker)
                {
                    Checker check = boardDict[square];
                    DrawChecker(g, check);
                }
            }
        }

        //Draw a 50 x 50 square
        public void DrawSquare(Graphics g, Square s)
        {
            var pen = new Pen(Color.Black, 2);
            var brush = new SolidBrush(s.curColor);
            int x = s.x;
            int y = s.y;
            g.DrawRectangle(pen, x, y, 50, 50);
            g.FillRectangle(brush, x, y, 50, 50);
        }

        //Highlight a 50 x 50 square - by giving it a golden border
        public void HighlightChecker(Graphics g, Checker c)
        {
            var pen = new Pen(Color.Gold, 2);
            int x = c.x;
            int y = c.y;
            g.DrawRectangle(pen, x, y, 50, 50);
        }

        //Draw a circle in the middle of a 50 x 50 square
        public void DrawChecker(Graphics g, Checker c)
        {
            var brush = new SolidBrush(c.color);
            int x = c.x;
            int y = c.y;
            if(c.king) // if Checker is a king, right "King" in the drawn circle
            {
                string king = "King";
                System.Drawing.Font font = new System.Drawing.Font("Arial", 12);
                System.Drawing.SolidBrush kingBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
                System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();
                g.FillEllipse(brush, x, y, 50, 50);
                g.DrawString(king, font, kingBrush, x+6, y+15, drawFormat);
            }
            else
            {
                g.FillEllipse(brush, x, y, 50, 50);
            }
            
        }

        //remove checker from a given square
        public void removeChecker(Square s)
        {
            board.removeChecker(s);
        }

        //Check if the user has any checkers that should be marked as a king
        public void CheckForKing(Square s, Color check_color)
        {
            int y = s.y;

            //Red is king
            if(y == 0 && check_color == Color.Red)
            {
                //Get Checker at square
                Checker c = getChecker(s);
                c.setKing(true);
                updateSquare(s, c);


            }
            else if(y == 350 && check_color == Color.White) // white is king
            {
                //Get Checker at square
                Checker c = getChecker(s);
                c.setKing(true);
                updateSquare(s, c);
            }
        }

        //Draw the initial checkerboard and fill in the board dictionary
        public void InitalizeCheckerBoard(Game_Screen screen)
        {
            screen.Show();
            var g = screen.CreateGraphics();
            var blackPen = new Pen(Color.Black, 2);
            Dictionary<Square, Checker> boardDict = getBoard();
            foreach (var square in boardDict.Keys)
            {
                DrawSquare(g, square);
                if (square.checker)
                {
                    Checker check = boardDict[square];
                    DrawChecker(g, check);
                }
            }
        }

        //wrapper to call boards checkForWin function
        public bool checkForWin(Color color)
        {
            return board.checkForWin(color);
        }
    }
}
