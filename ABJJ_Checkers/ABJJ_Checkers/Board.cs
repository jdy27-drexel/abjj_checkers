﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;


namespace ABJJ_Checkers
{
    //Serializable so we can pass Board over network
    [Serializable]
    public class Board
    {
        //Internal board representation
        Dictionary<Square, Checker> boardDict = new Dictionary<Square, Checker>();
        
        //Sets up the internal board representation by generating the squares and checkers
        public Board()
        {
            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    Square s = null;
                    Checker check = null;
                    //Even y value
                    if (y % 2 == 0)
                    {
                        //Even x value
                        if (x % 2 == 0)
                        {
                            Color c = Color.DarkGray;
                            s = new Square(50 * x, 50 * y, c);
                        }
                        else
                        {
                            Color c = Color.LightGray;
                            s = new Square(50 * x, 50 * y, c);
                            if (y < 3)//If on light gray space and y < 3 or y > 4 we put checker on space aswell
                            {
                                c = Color.White;
                                check = new Checker(s.x, s.y, c);
                                s.setChecker(true);
                            }
                            else if (y > 4)
                            {
                                c = Color.Red;
                                check = new Checker(s.x, s.y, c);
                                s.setChecker(true);
                            }
                        }


                    }
                    else// odd y value
                    {
                        //Even X value
                        if (x % 2 == 0)
                        {
                            Color c = Color.LightGray;
                            s = new Square(50 * x, 50 * y, c);
                            if (y < 3)//If on light gray space and y < 3 or y > 4 we put checker on space aswell
                            {
                                c = Color.White;
                                check = new Checker(s.x, s.y, c);
                                s.setChecker(true);
                            }
                            else if (y > 4)
                            {
                                c = Color.Red;
                                check = new Checker(s.x, s.y, c);
                                s.setChecker(true);
                            }
                        }
                        else
                        {
                            Color c = Color.DarkGray;
                            s = new Square(50 * x, 50 * y, c);
                        }

                    }
                    boardDict.Add(s, check);
                }
            }
        }

        //Gets the square at the specified x and y coordinate (cordinate for a square is top-left corner)
        public Square getSquare(int x, int y)
        {
            foreach (Square s in boardDict.Keys)
            {
                if (s.x == x && s.y == y)
                    return s;
            }
            return null;
        }

        //Get the checker (or null) for a specified Square
        public Checker getChecker(Square s)
        {
            return boardDict[s];
        }

        //Given a checker, returns a List of Tuple<Square,Square> that specify the legal moves it can make
        //The first Square in the tuple is the position it can move to, the second Square has a value if
        //we are jumping over a square, else it is null
        public List<Tuple<Square, Square>> moveValidator(Checker c)
        {
            List<Tuple<Square, Square>> ret = new List<Tuple<Square, Square>>();
            int y, x1, x2;

            if (c.color == Color.Red || c.king) //For red checkers, or king as king goes both ways
            {
                y = c.y - 50;
                x1 = c.x - 50;
                x2 = c.x + 50;

                foreach (Square s in boardDict.Keys)
                {
                    if (s.y == y && s.x == x1) //Upper left space
                    {
                        if (!s.checker) //If no checker on space, its a move and we add
                        {
                            Tuple<Square, Square> tuple = new Tuple<Square, Square>(s, null);
                            ret.Add(tuple);
                        }
                        else if(boardDict[s].color != c.color) //If space has different color check, get next space to see if jump possible
                        {
                            int tempy = s.y - 50;
                            int tempx = s.x - 50;

                            Square s2 = this.getSquare(tempx, tempy);
                            if (s2 != null && !s2.checker)
                            {
                                //Jump is possible
                                Tuple<Square, Square> tuple = new Tuple<Square, Square>(s2, s);
                                ret.Add(tuple);
                            }
                        }
                    }
                    else if (s.y == y && s.x == x2)  //Upper right space
                    {
                        if (!s.checker)
                        {
                            Tuple<Square, Square> tuple = new Tuple<Square, Square>(s, null);
                            ret.Add(tuple);
                        }
                        else if (boardDict[s].color != c.color) //If space has different color check, get next space to see if jump possible
                        {
                            int tempy = s.y - 50;
                            int tempx = s.x + 50;

                            Square s2 = this.getSquare(tempx, tempy);
                            if (s2 != null && !s2.checker)
                            {
                                //Jump is possible
                                Tuple<Square, Square> tuple = new Tuple<Square, Square>(s2, s);
                                ret.Add(tuple);
                            }
                        }
                    }
                }
            }
            if (c.color == Color.White || c.king) //For White checkers, or king as king goes both ways
            {
                y = c.y + 50;
                x1 = c.x - 50;
                x2 = c.x + 50;

                foreach (Square s in boardDict.Keys)
                {
                    if (s.y == y && s.x == x1) //Lower left space
                    {
                        if (!s.checker)
                        {
                            Tuple<Square, Square> tuple = new Tuple<Square, Square>(s, null);
                            ret.Add(tuple);
                        }
                        else if (boardDict[s].color != c.color) //If space has different color check, get next space to see if jump possible
                        {
                            int tempy = s.y + 50;
                            int tempx = s.x - 50;

                            Square s2 = this.getSquare(tempx, tempy);
                            if (s2 != null && !s2.checker)
                            {
                                //Jump is possible
                                Tuple<Square, Square> tuple = new Tuple<Square, Square>(s2, s);
                                ret.Add(tuple);
                            }
                        }
                    }
                    else if (s.y == y && s.x == x2) //Lower right space
                    {
                        if (!s.checker)
                        {
                            Tuple<Square, Square> tuple = new Tuple<Square, Square>(s, null);
                            ret.Add(tuple);
                        }
                        else if (boardDict[s].color != c.color) //If space has different color check, get next space to see if jump possible
                        {
                            int tempy = s.y + 50;
                            int tempx = s.x + 50;

                            Square s2 = this.getSquare(tempx, tempy);
                            if (s2 != null &&  !s2.checker)
                            {
                                //Jump is possible
                                Tuple<Square, Square> tuple = new Tuple<Square, Square>(s2, s);
                                ret.Add(tuple);
                            }
                        }
                    }
                }

            }
            return ret;
        }

        //Checks the Board it see if a certain color has won
        public bool checkForWin(Color check_color)
        {
            foreach(Square s in boardDict.Keys)
            {
                if (boardDict[s] != null && boardDict[s].color == check_color)
                {
                    return false;
                }
            }
            return true;
        }

        //Update the board by switching the checker from -> to
        public void updateBoard(Square to, Square from)
        {
            boardDict[to] = boardDict[from];
            boardDict[from] = null;
            boardDict[to].x = to.x;
            boardDict[to].y = to.y;
            from.checker = false;
            to.checker = true;
        }
        //Returns internal board representation
        public Dictionary<Square, Checker> getBoard()
        {
            return boardDict;
        }
        //Sets the internal representation to passed in value
        public void setBoard(Dictionary<Square, Checker> nb)
        {
            this.boardDict = nb;
        }
        //Removes checker from square s
        public void removeChecker(Square s)
        {
            this.boardDict[s] = null;
            s.checker = false;
        }
        //Update square to now hold checker c
        public void updateSquare(Square s, Checker c)
        {
            boardDict[s] = c;
        }
    }
}
