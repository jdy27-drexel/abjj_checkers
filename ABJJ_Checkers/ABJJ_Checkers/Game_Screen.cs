﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ABJJ_Checkers
{
    public partial class Game_Screen : Form
    {
        #region Variables
        //bool for if it is current forms turn
        public bool turn;
        //Specifies if a piece is already selected
        bool selected = false;
        //Specifies color form is playing as
        Color mycolor;
        //GameHelper used for helper functions
        GameHelpers gh;
        //List of moves a selected checker can make
        List<Tuple<Square,Square>> moves;
        //Location selected checker is currently at
        Square curLocation;
        //Serialized board that will be sent to other player
        public byte[] moveToSend = null;
        #endregion Variables

        //Constructor
        public Game_Screen(GameHelpers gh, Color color, bool turn)
        {
            InitializeComponent();
            this.gh = gh;
            this.mycolor = color;
            this.turn = turn;
            this.MinimumSize = new Size(500, 500);
        }

        //Where main portion of game code is
        //Based on variables and where one clicks, will allow the user to play
        //a game of checkers with another player
        private void TestGraph_MouseClick(object sender, MouseEventArgs e)
        {
            //Get graphics for drawing squares/checkers
            var g = this.CreateGraphics();
            int x, y;
            bool jumped = false;

            if(turn)//If its our turn
            {
                if(!selected)//No piece selected
                {
                    
                    x = e.X;
                    y = e.Y;
                    //Get square at clicked location
                    Square s = gh.getSquare(x, y);

                    if(s == null)
                    {
                        return;
                    }

                    if(s.checker)//If checker at square
                    {
                        Checker c = gh.getChecker(s);
                        if(c.color == mycolor)//Checker is color we can move
                        {
                            moves = gh.moveValidator(c);//Check for possible moves
                            if(moves.Count > 0)//If there are moves
                            {
                                //Set current location and selected variable
                                curLocation = s;
                                this.selected = true;
                                //Highlight possible move locations
                                foreach (Tuple<Square,Square> t in moves)
                                {
                                    t.Item1.curColor = Color.Gold;
                                    gh.DrawSquare(g, t.Item1);
                                }
                                //Highlight selected checker
                                gh.HighlightChecker(g, c);
                            }
                        }
                    }
                }
                else //Selected
                {
                    x = e.X;
                    y = e.Y;
                    Square s = gh.getSquare(x, y);//Get square that was clicked on
                    bool moved = false;

                    //Looking at possible move locations to see if clicked on square is one
                    foreach (Tuple<Square, Square> t in moves)
                    {
                        if (s == t.Item1)
                        {
                            gh.updateBoard(s, curLocation);
                            //Check if move jumped a checker
                            if(t.Item2 != null)
                            {
                                //If we did, remove checker and redraw the square it was on
                                gh.removeChecker(t.Item2);
                                Square s2 = gh.getSquare(t.Item2.x, t.Item2.y);
                                gh.DrawSquare(g, s2);
                                jumped = true;
                            }
                            moved = true;
                        }
                    }

                    if(moved)//If we did move
                    {
                        //Returned highlighted squares to base color
                        foreach (Tuple<Square, Square> t in moves)
                        {
                            t.Item1.curColor = t.Item1.baseColor;
                            gh.DrawSquare(g, t.Item1);
                        }

                        //Redrawing Checker
                        gh.CheckForKing(s,mycolor);
                        Checker ch = gh.getChecker(s);                       
                        gh.DrawChecker(g, ch);
                        gh.DrawSquare(g, curLocation);

                        if (jumped)//If move was a jump, see if another jump can be made
                        {
                            //Find moves for checker at new location                           
                            moves = gh.moveValidator(ch);

                            bool canMove = false;

                            //Check to see if any moves invlove a jump
                            //If they do, the checker will move again immediately
                            foreach (Tuple<Square, Square> t in moves)
                            {
                                if (t.Item2 != null)
                                {
                                    //Highlight squares that are possible moves
                                    t.Item1.curColor = Color.Gold;
                                    gh.DrawSquare(g, t.Item1);
                                    canMove = true;
                                }
                            }

                            //Checker can move
                            if (canMove == true)
                            {
                                //Highlight checker, and set new curLocation it its location
                                gh.HighlightChecker(g, ch);
                                curLocation = gh.getSquare(ch.x, ch.y);
                            }
                            else//Checker Cannot move
                            {
                                //end of move
                                moved = false;
                                jumped = false;
                                resetAndMakeMove();
                            }
                        }
                        else//Move we made was not a jump
                        {
                            //end of move
                            moved = false;
                            jumped = false;
                            resetAndMakeMove();
                        }
                    }
                }                
            }
            else//Not our turn, do nothing
            {
                return;
            }                         
        }

        public void resetAndMakeMove()
        {
            curLocation = null;
            selected = false;
            turn = false;
            //Our internal board is updated, so serialize it and send it to other player
            moveToSend = gh.MakeMove();
        }
    }
}
