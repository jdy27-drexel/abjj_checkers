﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace ABJJ_Checkers
{
    public class NetworkConnector
    {
        #region Variables
        //TCP Object used to host a game
        private TcpListener listener;

        //TCP Object used to join a game
        //or to communicate between another client
        private TcpClient client;

        //Defines if object is the host
        private Boolean host;

        //Port that will be used to communicate
        private int port = 13000;
        #endregion Variables

        //Constructor
        public NetworkConnector()
        {
            //Initalize
            listener = null;
            client = null;
            host = false;
        }

        //Gets the IP address that will be used to host the game
        public string GetIPAddress()
        {
            var ips = Dns.GetHostEntry(Dns.GetHostName());
            List<IPAddress> useable_ips = new List<IPAddress>();
            foreach (var ip in ips.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    var ip_string = ip.ToString();
                    //We do not use IP addresses that end in .1 as they are associated with a vm
                    if(ip_string.Substring(ip_string.Length -2) != ".1")
                    {
                        useable_ips.Add(ip);
                    }
                    
                }
            }
            if(useable_ips.Count == 0)
            {
                return "ERROR";
            }
            return useable_ips.First().ToString();
        }
        
        //Starts a listener object for hosting a game
        //returns a task that finishes when "joiner" joins the game
        public async Task<bool> HostGame()
        {
            //Find an acceptable IP to host game on
            var ip = GetIPAddress();

            var address = IPAddress.Parse(ip);

            //Start the listener for the other to join
            if (listener == null)
            {
                listener = new TcpListener(address, port);
            }
            listener.Start();
            client = await listener.AcceptTcpClientAsync();
            listener.Stop();
            host = true;
            return true;
        }

        //Attempt to join a game using specified ip
        public Boolean JoinGame(string IP)
        {
            //If We are not host and are currently not connected to a game we can join one
            if(!host && client == null)
            {
                try
                {
                    client = new TcpClient(IP, port);
                }
                catch (Exception e)
                {
                    return false;
                }
                return true;
            }
            return false;           
        }

        //Read a move that was sent as string
        public string ReadMove()
        {
            Byte[] data = new Byte[30000];
            var stream = client.GetStream();
            //Can make async, which may help with freezing
            //Read bytes from the stream
            int bytes_read = stream.Read(data, 0, data.Length);
            //Translate byes into string using ASCII encoding
            string move = System.Text.Encoding.ASCII.GetString(data, 0, bytes_read);
            return move;
        }



        //Read a move that was sent as bytes
        //In this case, serialized board is received
        public byte[] ReadMoveBytes()
        {
            Byte[] data = new Byte[30000];
            var stream = client.GetStream();
            //Can make async, which may help with freezing
            //Read bytes from the stream
            int bytes_read = stream.Read(data, 0, data.Length);
            //Translate byes into string using ASCII encoding
            return data;
        }

        //Sends a move to the other player as string
        public Boolean SendMove(String move)
        {
            try
            {
                var stream = client.GetStream();
                //Convert the move string into
                Byte[] data = System.Text.Encoding.ASCII.GetBytes(move);
                //Send the move
                stream.Write(data, 0, data.Length);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //Sends a move to the other player as bytes
        //In this case, serialized board with be sent
        public Boolean SendMove(byte[] move)
        {
            try
            {
                var stream = client.GetStream();
                stream.Write(move, 0, move.Length);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        //closes the connection
        public Boolean CloseGame()
        {
           // try
            //{
                client.Close();
                if(listener != null)
                {
                    listener.Stop();
                    listener = null;
                }
                return true;
            //}
          /*  catch (Exception e)
            {
                return false;
            }*/
        }
        //Checks to see if we are still connected to other player
        public Boolean IsConnected()
        {
            return client.Connected;
        }

        //Returns if object is a host
        public Boolean IsHost()
        {
            return host;
        }
    }
}
