﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ABJJ_Checkers
{
    public partial class IPPopup : Form
    {
        private bool submit_Clicked = false;

        public IPPopup()
        {
            InitializeComponent();
            SubmitButton.Visible = false;
            IPTextbox.ReadOnly = true;
            
        }
        public void SetTextboxValue(String value)
        {
            IPLabel.Visible = true;
            IPLabel.Text = value;
            IPTextbox.Visible = false;
        }

        public string GetEnterIP()
        {
            //Setup our submit button
            SubmitButton.Visible = true;
            IPTextbox.Text = "";
            IPTextbox.ReadOnly = false;


            //Display ourselves
            this.ShowDialog();
            var IP = IPTextbox.Text;

            //Cleanup
            SubmitButton.Visible = false;
            IPTextbox.Text = "";
            IPTextbox.ReadOnly = true;
            submit_Clicked = false;

            return IP;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            submit_Clicked = true;
            this.Close();
        }
    }
}
